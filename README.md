# Checklist

## Este proyecto busca establecer una forma de trabajo robusta y eficiente, la que incluye 4 secciones ligadas al proceso de vida de un proyecto de machine learning


🔵 = listo

⚪️ = no listo

## A. Data Tests [⚪️⚪️⚪️⚪️⚪️]

1. [ ] Feature expectations are captured in a schema. [Link a esquema o json de validación]()
2. [ ] All features are beneficial and cost is not too much [Link a proceso de prueba de modelo con menos variables]()
3. [ ] Data pipeline has appropriate privacy controls (se controló que las variables estuviesen anonimizadas)
4. [ ] New features can be added quickly
5. [ ] All input feature code is tested (unit tests para lectura y creación de nuevas variables)

## B. Model Tests [⚪️🔵️⚪️⚪️⚪️⚪️]

1. [ ] Model specs are reviewed (hubo revisión de código)
2. [✔] All relevant hyperparameters have been tuned
3. [ ] The impact of model staleness is known [Link a experimento con modelos más nuevos/antiguos]()
4. [ ] A simpler model is not better [Link a test con modelo más simple: líneal o pocas variables]()
5. [ ] Model quality is sufficient on important data slices [Link a ejercicio en al menos una variable relevante]()
6. [ ] The model is tested for considerations of inclusion (ML fairness) [Link a análisis: variables de entrada se correlacionan con categorías protegidas?]()

## C. ML Infrastructure tests [⚪️⚪️⚪️⚪️⚪️]

1. [ ] Training is reproducible (inclusión de seeds, probar que ejecutar varias veces produce lo mismo)
2. [ ] The ML pipeline is integration tested (al menos: se probó correr el script de entrenamiento completo)
3. [ ] Model quality is validated before serving [Link a checks de outputs]()
4. [ ] The model is debuggable [Link a análisis con SHAP, LIME o algo similar]()
5. [ ] El modelo cumple con casos emblemáticos[Link a test con casos que sabemos que tienen que ser de cierta manera]()

## D. Monitoring tests [⚪️⚪️⚪️⚪️⚪️⚪️⚪️]

1. [ ] Data invariants hold for inputs (validación de inputs en tablero)
2. [ ] Training and serving are not skewed (se guardan datos recibidos en línea y se comparan con histórico)
3. [ ] Models are not too stale (se guardan datos recibidos y se comprueba si decae calidad en el tiempo)
4. [ ] Models are numerically stable (se registra aparición de valores extremos de output (incluído NaN) en tablero)
5. [ ] Computing performance has not regressed (se registra rendimiento computacional en el tiempo: uso de RAM, latencia, uptime)
6. [ ] Guardar etiqueta real de datos nuevos (cuando vayan apareciendo) y graficarlo
7. [ ] Medir promedio de predicciones para diferentes segmentos
